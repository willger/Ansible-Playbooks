# Playbooks

Playbooks for managing components in my home network and applications. 

## Running Playbooks

```command
ansible-playbook -l [target] -i [inventory file] -u [remote user] path/to/playbook.yml
```
