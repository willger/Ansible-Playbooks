# Apache on Ubuntu 18.04

These playbooks will install and provision the Apache 2 web server as a proxy. Virtualhosts can be created by running any `site-[name].yml` playbook with the options specified for that site in the in the coorisponding `vars/var-[site].yml` variable file. Global Appache settings are in the `vars/var_apache.yml` variabale file. Golbal settings are imported in all site specific playbooks.

## Settings

- `serverAdmin`: email address for server admin


## Running this Playbook

```command
ansible-playbook -l [target] -i [inventory file] -u [remote user] playbook.yml
```
